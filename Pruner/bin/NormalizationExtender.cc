#include "NormalizationExtender.h"

#include <TTree.h>


NormalizationExtender::NormalizationExtender()
    : outputFile_{nullptr},
      eventsRead_{0},
      sumGenWeights_{0.}, sumGenWeightsPassed_{0.},
      sumLheWeights_{0.}, sumLheWeightsPassed_{0.},
      pileupProfile_{"PileupProfile", "Pileup profile", 100, 0., 100.} {
  pileupProfile_.SetDirectory(nullptr);
}


void NormalizationExtender::Fill(bool passed) {
  ++eventsRead_;
  sumGenWeights_ += *src_->genWeight;
  sumLheWeights_ += *src_->lheWeight;
  if (passed) {
    sumGenWeightsPassed_ += *src_->genWeight;
    sumLheWeightsPassed_ += *src_->lheWeight;
  }

  pileupProfile_.Fill(*src_->pileupTrueInteractions);
}


void NormalizationExtender::Initialize(TTreeReader &reader, TFile *outputFile) {
  src_.emplace(reader);
  outputFile_ = outputFile;
}


void NormalizationExtender::Write() {
  // Save event counter and sums of event weights in a tree. Convert the sums of
  // weights into Float_t for storage.
  TTree tree("EventCounts", "Numbers of events and sums of weights");
  tree.SetDirectory(outputFile_);

  Float_t sumGenWeightsF = sumGenWeights_;
  Float_t sumGenWeightsPassedF = sumGenWeightsPassed_;
  Float_t sumLheWeightsF = sumLheWeights_;
  Float_t sumLheWeightsPassedF = sumLheWeightsPassed_;

  tree.Branch("InEvtCount", &eventsRead_);
  tree.Branch("InGeneratorWeightSum", &sumGenWeightsF);
  tree.Branch("EvtGeneratorWeightSum", &sumGenWeightsPassedF);
  tree.Branch("InLheEvtWeightSum", &sumLheWeightsF);
  tree.Branch("LheEvtWeightSum", &sumLheWeightsPassedF);
  tree.Fill();
  outputFile_->WriteObject(&tree, tree.GetName());

  pileupProfile_.SetDirectory(outputFile_);
  outputFile_->WriteObject(&pileupProfile_, pileupProfile_.GetName());
}


NormalizationExtender::Source::Source(TTreeReader &reader)
    : genWeight{reader, "Generator_weight"},
      lheWeight{reader, "LHEWeight_originalXWGTUP"},
      pileupTrueInteractions{reader, "Pileup_nTrueInt"} {}

