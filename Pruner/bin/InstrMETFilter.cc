#include "InstrMETFilter.h"

#include <boost/filesystem.hpp>
#include <yaml-cpp/yaml.h>

namespace fs = boost::filesystem;


InstrMETFilter::InstrMETFilter(std::string const &triggerConfigPath,
                               std::string const &year)
    : idBranchName_{"Photon_cutBased"} {

  if (triggerConfigPath.empty())  // By default the path is set to ""
    throw std::runtime_error(
        "Path to trigger configuration file was not provided.");
  if (not fs::exists(triggerConfigPath)) {
    std::ostringstream message;
    message << "Trigger configuration file \""
        << triggerConfigPath << "\" doesn't exist.";
    throw std::runtime_error(message.str());
  }

  auto const triggerConfig =
    YAML::LoadFile(triggerConfigPath)["InstrMET"][year];
  if (not triggerConfig) {
    std::ostringstream message;
    message << "Trigger configuration file \"" << triggerConfigPath
        << "\" doesn't contain entry for year \"" << year << "\".";
    throw std::runtime_error(message.str());
  }

  triggerFilter_ = TriggerFilter(triggerConfig.as<std::vector<std::string>>());
}


bool InstrMETFilter::Filter() const {
  if (not triggerFilter_())
    return false;

  int goodPhotons = 0;

  for (int iPhot = 0; iPhot < int(*src_->photonNum); iPhot++) {
    if (src_->photonPt[iPhot] < 50)  // In the analysis: 55
      continue;

    // Select tight photon.
    if (not (IsTightPhoton(src_->photonCutBased[iPhot]) or src_->photonMvaId[iPhot]))
      continue;

    goodPhotons++;
  }

  return (goodPhotons > 0);
}


void InstrMETFilter::Initialize(TTreeReader &reader) {
  triggerFilter_.Init(reader);
  src_.emplace(reader, idBranchName_);
}


InstrMETFilter::Source::Source(TTreeReader &reader,
                               std::string const &idBranchName)
    : photonNum{reader, "nPhoton"},
      photonPt{reader, "Photon_pt"},
      photonCutBased{reader, idBranchName.c_str()},
      photonMvaId{reader, "Photon_mvaID_WP80"} {}


bool InstrMETFilter::IsTightPhoton(int photonID) {
  return (photonID == 3);
}
