#ifndef TRIGGERFILTER_H_
#define TRIGGERFILTER_H_

#include <memory>
#include <string>
#include <vector>

#include <TTreeReader.h>
#include <TTreeReaderValue.h>


/**
 * \brief Selects events accepted by OR of a set of triggers
 *
 * An event passes this filter if it is accepted by at least one of the triggers
 * in the set. Triggers that do not correspond to existing branches in the
 * NanoAOD event tree are ignored.
 */
class TriggerFilter {
 public:
  TriggerFilter() noexcept = default;

  /// Constructor from a set of trigger names
  TriggerFilter(std::vector<std::string> const &triggers);

  /// Peforms initialization for provided reader
  void Init(TTreeReader &reader);

  /**
   * \brief Evaluates filter decision
   *
   * Returns true if at least one of the triggers has fired.
   */
  bool operator()() const;

  /// Sets namses of triggers to be used
  void SetTriggers(std::vector<std::string> const &triggers);

 private:
  /**
   * \brief Names of requested triggers
   *
   * They are also interpreted as names of branches in the NanoAOD event tree.
   */
  std::vector<std::string> triggerNames_;

  /// Objects that provide access to trigger decisions
  std::vector<std::unique_ptr<TTreeReaderValue<Bool_t>>> triggerBits_;
};

#endif  // TRIGGERFILTER_H_

