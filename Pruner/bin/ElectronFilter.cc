#include "ElectronFilter.h"

#include <algorithm>
#include <sstream>
#include <stdexcept>

#include <boost/filesystem.hpp>
#include <yaml-cpp/yaml.h>

namespace fs = boost::filesystem;


ElectronFilter::ElectronFilter(
    std::string const &subSelection, std::string const &triggerConfigPath,
    std::string const &year, int verbosity) {
  if (triggerConfigPath.empty())  // By default the path is set to ""
    throw std::runtime_error(
        "Path to trigger configuration file was not provided.");
  if (not fs::exists(triggerConfigPath)) {
    std::ostringstream message;
    message << "Trigger configuration file \""
        << triggerConfigPath << "\" doesn't exist.";
    throw std::runtime_error(message.str());
  }

  auto const triggerConfig =
    YAML::LoadFile(triggerConfigPath)["Electron"][year];
  if (not triggerConfig) {
    std::ostringstream message;
    message << "Trigger configuration file \"" << triggerConfigPath
        << "\" doesn't contain entry for year \"" << year << "\".";
    throw std::runtime_error(message.str());
  }
  if (not triggerConfig.IsSequence()) {
    std::ostringstream message;
    message << "In trigger configuration file \"" << triggerConfigPath
        << "\", expected entry for year \"" << year << "\" to be a sequence.";
    throw std::runtime_error(message.str());
  }

  std::vector<std::string> triggersAccept, triggersVeto;
  for (auto const &groupNode : triggerConfig) {
    if (not groupNode.IsMap() or groupNode.size() != 1) {
      std::ostringstream message;
      message << "In trigger configuration file \"" << triggerConfigPath
          << "\", expected every element in entry for year \"" << year
          << "\" to be a map containing a single element.";
      throw std::runtime_error(message.str());
    }
    auto const node = *groupNode.begin();
    auto const groupName = node.first.as<std::string>();
    auto const groupTriggers = node.second.as<std::vector<std::string>>();

    if (subSelection == "MC") {
      // In simulation just use an OR of all triggers in the configuration
      triggersAccept.insert(
          triggersAccept.end(), groupTriggers.begin(), groupTriggers.end()
      );
    } else if (groupName == subSelection) {
      triggersAccept = groupTriggers;
      break;
    } else {
      // All trigger groups placed before the group corresponding to the
      // requested selection in data should be vetoed
      triggersVeto.insert(
          triggersVeto.end(), groupTriggers.begin(), groupTriggers.end());
    }
  }

  if (triggersAccept.empty()) {
    std::ostringstream message;
    message << "In trigger configuration file \"" << triggerConfigPath
        << "\", the entry for year \"" << year << "\" doesn't contain "
        << "triggers for requested subselection \"" << subSelection << "\".";
    throw std::runtime_error(message.str());
  }
  triggersAccept_.SetTriggers(triggersAccept);
  triggersVeto_.SetTriggers(triggersVeto);

  if (verbosity >= 2) {
    std::cout << "Accept triggers:";
    for (auto const &trigger : triggersAccept)
      std::cout << " " << trigger;
    std::cout << "\nVeto triggers:";
    for (auto const &trigger : triggersVeto)
      std::cout << " " << trigger;
    std::cout << std::endl;
  }
}


bool ElectronFilter::Filter() const {
  // Find good electrons that pass the pt threshold. Include a 10%
  // margin in pt to allow applying pt scale calibration and evaluating its
  // uncertainties.
  double const minPt = 40.;
  std::vector<TLorentzVector> electronMomenta;

  for (int i = 0; i < int(*src_->elNum); ++i) {
    if (src_->elPt[i] < 0.9 * minPt)
      continue;

    bool const newId = src_->elMvaId[i];
    if (not newId)
      continue;

    TLorentzVector p4;
    p4.SetPtEtaPhiM(src_->elPt[i], src_->elEta[i], src_->elPhi[i],
                    src_->elMass[i]);
    electronMomenta.emplace_back(p4);
  }

  if (electronMomenta.size() < 1)
    return false;

  return triggersAccept_() and not triggersVeto_();
}


void ElectronFilter::Initialize(TTreeReader &reader) {
  src_.emplace(reader);

  for (auto f : {&triggersAccept_, &triggersVeto_})
    f->Init(reader);
}


std::vector<std::string> ElectronFilter::SubSelections() {
  return {
      "MC", "SingleElectron"};
}


ElectronFilter::Source::Source(TTreeReader &reader)
    : elNum{reader, "nElectron"},
      // elCutBasedId{reader, "Electron_cutBased"},
      elMvaId{reader, "Electron_mvaFall17V2Iso_WP90"},
      // elRelIso{reader, "Electron_pfRelIso03_all"},
      elPt{reader, "Electron_pt"}, elEta{reader, "Electron_eta"},
      elPhi{reader, "Electron_phi"}, elMass{reader, "Electron_mass"} {}
