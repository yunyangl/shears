#ifndef SHEARS_PRUNER_BIN_ELECTRONFILTER_H_
#define SHEARS_PRUNER_BIN_ELECTRONFILTER_H_

#include <optional>
#include <string>
#include <vector>

#include <TLorentzVector.h>
#include <TTreeReaderArray.h>
#include <TTreeReaderValue.h>

#include "FilterBase.h"
#include "TriggerFilter.h"


class ElectronFilter : public FilterBase {
 public:
  ElectronFilter(
      std::string const &subSelection,
      std::string const &triggerConfigPath, std::string const &year,
      int verbosity = 0);

  bool Filter() const override;
  void Initialize(TTreeReader &reader) override;

  /**
   * \brief Return supported subselections
   *
   * The subselections are not checked against what is actually included in the
   * configuration file.
   */
  static std::vector<std::string> SubSelections();

 private:
  /**
   * \brief Auxiliary structure to simplify delayed initialization of branches
   * read from source tree
   */
  struct Source {
    Source(TTreeReader &reader);

    TTreeReaderValue<UInt_t> elNum;
    // TTreeReaderArray<Int_t> elCutBasedId;
    TTreeReaderArray<Bool_t> elMvaId;
    // TTreeReaderArray<Float_t> elRelIso;
    TTreeReaderArray<Float_t> elPt, elEta, elPhi, elMass;
  };

  /// Trigger filter that must be passed by event for it to be selected
  TriggerFilter triggersAccept_;

  /// Trigger filter that must not be passed by event for it to be selected
  TriggerFilter triggersVeto_;

  mutable std::optional<Source> src_;
};

#endif  // SHEARS_PRUNER_BIN_ELECTRONFILTER_H_
