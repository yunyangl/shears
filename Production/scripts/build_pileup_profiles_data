#!/usr/bin/env python

"""Constructs pileup profiles in data."""

import argparse
import itertools
import json
import os
import subprocess
from uuid import uuid4

import ROOT

from shears.Production.config import Config


# Collision runs for all eras
ERAS = {
    '2016B': (272007, 275376),
    '2016C': (275657, 276283),
    '2016D': (276315, 276811),
    '2016E': (276831, 277420),
    '2016F': (277772, 278808),
    '2016G': (278820, 280385),
    '2016H': (280919, 284044),

    '2017B': (297046, 299329),
    '2017C': (299368, 302029),
    '2017D': (302030, 303434),
    '2017E': (303824, 304797),
    '2017F': (305040, 306462),

    '2018A': (315252, 316995),
    '2018B': (317080, 319310),
    '2018C': (319337, 320065),
    '2018D': (320673, 325175)
}


def filter_mask(mask, run_range):
    """Filter lumi mask only keeping runs within the given range.

    Args:
        mask:  Dictionary with parsed lumi mask.
        run_range:  Tuple representing the range of runs to keep.
            Boundaries are included.
    Return:
        Dictionary with filtered lumi mask.
    """

    filtered_mask = {}
    for run_key, lumi_sections in mask.items():
        if not run_range[0] <= int(run_key) <= run_range[1]:
            continue
        filtered_mask[run_key] = lumi_sections
    return filtered_mask


def process_period(mask, pileup_path):
    """Construct pileup profiles for given mask.

    Produce the nominal profile as well as the profiles for the
    systematic variation. Run these three jobs in parallel.

    Args:
        mask:  Dictionary with parsed lumi mask.
        pileup_path:  Path to file with per-LS pileup.
    Return:
        Dictionary with ROOT histograms representing pileup profiles for
        each variation, with the variation label as the key.
    """

    mask_path = uuid4().hex + '.json'
    with open(mask_path, 'w') as f:
        json.dump(mask, f)

    # Pileup cross section and its uncertainty as recommended in [1]
    # [1] https://twiki.cern.ch/twiki/bin/view/CMS/PileupJSONFileforData?rev=40#Recommended_cross_section
    xsec_nominal = 69.2e3  # ub
    xsec_uncertainty = 0.046  # Relative

    processes = []
    output_files = {}
    for variation, xsec in [
        ('nominal', xsec_nominal),
        ('up', xsec_nominal * (1 + xsec_uncertainty)),
        ('down', xsec_nominal * (1 - xsec_uncertainty))
    ]:
        file_name = uuid4().hex + '.root'
        process = subprocess.Popen([
            'pileupCalc.py', '-i', mask_path, '--inputLumiJSON', pileup_path,
            '--calcMode', 'true', '--minBiasXsec', str(xsec),
            '--maxPileupBin', '100', '--numPileupBins', '100',
            file_name
        ])
        processes.append(process)
        output_files[variation] = file_name

    for process in processes:
        process.wait()
        if process.returncode != 0:
            raise RuntimeError('pileupCalc.py terminated with an error.')

    profiles = {}
    for variation, path in output_files.items():
        input_file = ROOT.TFile(path)
        profile = input_file.Get('pileup')
        profile.SetDirectory(None)
        input_file.Close()
        profile.SetName(variation)
        profiles[variation] = profile

    for path in itertools.chain(output_files.values(), [mask_path]):
        os.remove(path)

    return profiles


if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser(description=__doc__)
    arg_parser.add_argument(
        '-i', '--mask', required=True, help='JSON file with luminosity mask.')
    arg_parser.add_argument(
        '-p', '--pileup', required=True, help='File with per-LS pileup.')
    arg_parser.add_argument(
        '-o', '--output', default='pileup_profiles_data.root',
        help='Name for output file with pileup profiles.')
    arg_parser.add_argument(
        '-c', '--config', help='Configuration file for skimming campaign. '
        'If given, will copy output file to appropriate directory in the '
        'stageout area.')
    args = arg_parser.parse_args()

    with open(args.mask) as f:
        mask = json.load(f)

    profiles = {}
    for era_label, run_range in ERAS.items():
        filtered_mask = filter_mask(mask, run_range)
        if not filtered_mask:
            continue
        profiles[era_label] = process_period(filtered_mask, args.pileup)

    output_file = ROOT.TFile(args.output, 'recreate')
    era_dirs = []
    for era_label in sorted(profiles.keys()):
        era_profiles = profiles[era_label]
        era_dir = output_file.mkdir(era_label)
        era_dirs.append(era_dir)  # Protect against garbage collection

        # Save run range for this era. The only way to do this is to use
        # a TVectorD.
        era_dir.cd()
        run_range = ERAS[era_label]
        write_run_range = ROOT.TVectorD(2)
        for i in range(2):
            write_run_range[i] = run_range[i]
        write_run_range.Write('run_range')

        for _, profile in era_profiles.items():
            profile.SetDirectory(era_dir)

    output_file.Write()
    output_file.Close()

    if args.config:
        config = Config(args.config)
        pileup_dir = config.pileup_dir().srm
        subprocess.check_call(['gfal-mkdir', '-p', pileup_dir])
        subprocess.check_call([
            'gfal-copy', '-f', args.output,
            os.path.join(pileup_dir, args.output)
        ])
        os.remove(args.output)
